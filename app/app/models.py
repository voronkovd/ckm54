# -*- coding: utf-8 -*
from uuid import uuid4

from django.conf import settings
from django.db import models
from image_cropping import ImageRatioField
import os
from easy_thumbnails.files import get_thumbnailer


def path_and_rename(path):
    def wrapper(instance, filename):
        ext = filename.split('.')[-1]
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join(path, filename)

    return wrapper


class Clinics(models.Model):
    name = models.CharField(max_length=128, verbose_name=u'Название', help_text=u'например: Клиника Мешалкина')
    phone = models.CharField(max_length=128, verbose_name=u'Телефон', help_text=u'например: +7 (383) 100 10 10')
    address = models.CharField(max_length=128, verbose_name=u'Адрес', help_text=u'например: ул. Крылова, 1а')
    timetable = models.CharField(max_length=128, verbose_name=u'График', help_text=u'например: с 08:00 до 20:00')
    email = models.EmailField(verbose_name=u"Email", help_text=u'Адрес для записи на прием')
    description = models.TextField(verbose_name=u"Описание")
    map = models.TextField(verbose_name=u"Код", help_text=u'https://tech.yandex.ru/maps/tools/constructor/', null=True)
    price = models.FileField(verbose_name=u"Прайс", upload_to=path_and_rename('files/'), help_text=u'*.xls')
    logo = models.ImageField(verbose_name=u'Лого', upload_to=path_and_rename('images/'), help_text=u'230x60')
    logo_s = models.ImageField(verbose_name=u'Лого меньше', upload_to=path_and_rename('images/'), help_text=u'230x60')

    class Meta:
        db_table = 'clinics'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Клинику'
        verbose_name_plural = u'Клиники'

    def __unicode__(self):
        return self.name

    def image_tag(self):
        file_logo = settings.MEDIA_ROOT + str(self.logo.__unicode__())
        if self.logo and os.path.isfile(file_logo):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.logo).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Логотип'

    image_tag.allow_tags = True


class ClinicImages(models.Model):
    clinic = models.ForeignKey(Clinics, verbose_name=u"Клиника")
    image = models.ImageField(verbose_name=u'Фото', upload_to=path_and_rename('images/'), help_text=u'В "О нас"')
    cropping = ImageRatioField('image', '293x214', verbose_name=u'Пропорции слайдера')
    cropping_full = ImageRatioField('image', '800x600', verbose_name=u'пропорции увеличенного фото')
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)

    class Meta:
        db_table = 'clinic_galleries'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Фотографию'
        verbose_name_plural = u'Фотографии'

    def __unicode__(self):
        return self.clinic.name

    def image_tag(self):
        file_image = settings.MEDIA_ROOT + str(self.image.__unicode__())
        if self.image and os.path.isfile(file_image):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.image).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Фото'

    image_tag.allow_tags = True


class Cabinets(models.Model):
    clinic = models.ForeignKey(Clinics, verbose_name=u"Клиника")
    name = models.CharField(max_length=128, verbose_name=u'Наименование кабинета')
    image = models.ImageField(verbose_name=u'Фото', upload_to=path_and_rename('images/'), help_text=u'Фото кабинета')
    cropping = ImageRatioField('image', '451x346', verbose_name=u'Пропорции слайдера')
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)

    class Meta:
        db_table = 'cabinets'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Кабинет'
        verbose_name_plural = u'Кабинеты'

    def __unicode__(self):
        return self.name

    def image_tag(self):
        file_image = settings.MEDIA_ROOT + str(self.image.__unicode__())
        if self.image and os.path.isfile(file_image):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.image).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Фото'

    image_tag.allow_tags = True


class CabinetSliders(models.Model):
    cabinet = models.ForeignKey(Cabinets, verbose_name=u"Кабинет", related_name='sliders')
    image = models.ImageField(verbose_name=u'Изображение', upload_to=path_and_rename('images/'))
    cropping = ImageRatioField('image', '451x346', verbose_name=u'Пропорции слайдера')
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)
    status = models.BooleanField(verbose_name=u'Опубликовать', default=False)

    class Meta:
        db_table = 'cabinet_sliders'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Слайд кабинета'
        verbose_name_plural = u'Слады кабинета'
        ordering = ('order', )

    def __unicode__(self):
        return self.cabinet.name

    def image_tag(self):
        file_image = settings.MEDIA_ROOT + str(self.image.__unicode__())
        if self.image and os.path.isfile(file_image):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.image).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Изображение'

    image_tag.allow_tags = True


class Services(models.Model):
    cabinet = models.ForeignKey(Cabinets, verbose_name=u"Кабинет", related_name='services')
    name = models.CharField(max_length=128, verbose_name=u'Наименование услугу')
    description = models.TextField(verbose_name=u"Описание")
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)

    class Meta:
        db_table = 'services'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Услугу'
        verbose_name_plural = u'Услуги'

    def __unicode__(self):
        return self.name


class Specialists(models.Model):
    clinic = models.ForeignKey(Clinics, verbose_name=u"Клиника")
    name = models.CharField(max_length=128, verbose_name=u'ФИО', help_text=u'например: Иванов Иван Иванович')
    role = models.CharField(max_length=128, verbose_name=u'Должность', help_text=u'например: Хирург', null=True)
    description = models.TextField(verbose_name=u"Информация")
    image = models.ImageField(verbose_name=u'Фото', upload_to=path_and_rename('images/'))
    cropping = ImageRatioField('image', '250x380', verbose_name=u'Пропорции слайдера')
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)

    class Meta:
        db_table = 'specialists'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Специалиста'
        verbose_name_plural = u'Специалисты'

    def __unicode__(self):
        return self.name

    def image_tag(self):
        file_image = settings.MEDIA_ROOT + str(self.image.__unicode__())
        if self.image and os.path.isfile(file_image):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.image).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Фото'

    image_tag.allow_tags = True


class Pages(models.Model):
    status = models.BooleanField(verbose_name=u'Опубликовать', default=False)
    clinic = models.ForeignKey(Clinics, verbose_name=u"Клиника")
    title = models.CharField(max_length=128, verbose_name=u'Наименование', help_text=u'например: Дисконтная система')
    image = models.ImageField(verbose_name=u'Фото', upload_to=path_and_rename('images/'))
    description = models.TextField(verbose_name=u"Страница")
    cropping = ImageRatioField('image', '500x300', verbose_name=u'Пропорции изображения')
    cropping_slide = ImageRatioField('image', '290x120', verbose_name=u'Пропорции слайдера')
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)

    class Meta:
        db_table = 'pages'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Страницу в раздел пациентам'
        verbose_name_plural = u'Пациентам'

    def __unicode__(self):
        return self.title

    def image_tag(self):
        file_image = settings.MEDIA_ROOT + str(self.image.__unicode__())
        if self.image and os.path.isfile(file_image):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.image).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Фото'

    image_tag.allow_tags = True


class Sliders(models.Model):
    clinic = models.ForeignKey(Clinics, verbose_name=u"Клиника")
    title = models.CharField(max_length=24, verbose_name=u'Наименование', help_text=u'например: Программы и акции')
    anons = models.TextField(verbose_name=u"Анонс", max_length=50)
    description = models.TextField(verbose_name=u"Информация")
    image = models.ImageField(verbose_name=u'Изображение', upload_to=path_and_rename('images/'))
    cropping = ImageRatioField('image', '290x120', verbose_name=u'Пропорции изображения')
    order = models.PositiveIntegerField(verbose_name=u'Сортировка', null=True)
    status = models.BooleanField(verbose_name=u'Опубликовать', default=False)

    class Meta:
        db_table = 'sliders'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Слайд'
        verbose_name_plural = u'Слады'

    def __unicode__(self):
        return self.title

    def image_tag(self):
        file_image = settings.MEDIA_ROOT + str(self.image.__unicode__())
        if self.image and os.path.isfile(file_image):
            options = {'size': (100, 100), 'crop': True, 'autocrop': True}
            thumb_url = get_thumbnailer(self.image).get_thumbnail(options).url
            return '<img src="' + thumb_url + '" />'

    image_tag.short_description = u'Изображение'

    image_tag.allow_tags = True