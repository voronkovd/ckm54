# -*- coding: utf-8 -*-
import os
import ConfigParser
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
from easy_thumbnails.conf import Settings as thumbnail_settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
custom_config = ConfigParser.ConfigParser()
custom_config.readfp(open(BASE_DIR + '/app/production.conf'))
SECRET_KEY = custom_config.get('main', 'secret_key')
DEBUG = custom_config.getboolean('main', 'debug')
TEMPLATE_DEBUG = custom_config.getboolean('main', 'template_debug')
ADMINS = ((custom_config.get('main', 'admin_name'), custom_config.get('main', 'admin_email')),)
ALLOWED_HOSTS = ['*']
DEFAULT_APP = custom_config.get('main', 'default_app')
INSTALLED_APPS = (
    'easy_thumbnails',
    'image_cropping',
    'suitlocale',
    'ckeditor',
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app',


)
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
TEMPLATE_CONTEXT_PROCESSORS = TCP + ('django.core.context_processors.request',)
ROOT_URLCONF = 'app.urls'
WSGI_APPLICATION = 'app.wsgi.application'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': custom_config.get('mysql', 'name'),
        'USER': custom_config.get('mysql', 'user'),
        'PASSWORD': custom_config.get('mysql', 'password'),
        'HOST': '127.0.0.1',
    }
}
TEMPLATE_DIRS = (
    BASE_DIR + '/app/templates/',
)
LANGUAGE_CODE = custom_config.get('main', 'language')
TIME_ZONE = custom_config.get('main', 'timezone')
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_ROOT = BASE_DIR + '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR + '/media/'
STATICFILES_DIRS = (
    # os.path.join(BASE_DIR, 'static'),
)
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SUIT_CONFIG = {
    'ADMIN_NAME': 'Админка',
    'SEARCH_URL': '',
    'HEADER_DATE_FORMAT': False,
    'HEADER_TIME_FORMAT': False,
    'CONFIRM_UNSAVED_CHANGES': False,
    'MENU_OPEN_FIRST_CHILD': True,
    'MENU': (
        {'label': u'Клиники', 'icon': 'icon-folder-close', 'url': '/admin/app/clinics/'},
        {'label': u'Галерея', 'icon': 'icon-folder-close', 'url': '/admin/app/clinicimages/'},
        {'label': u'Слайдер', 'icon': 'icon-folder-close', 'url': '/admin/app/sliders/'},
        {'label': u'Кабинеты', 'icon': 'icon-folder-close', 'url': '/admin/app/cabinets/'},
        {'label': u'Слайдер Кабинета', 'icon': 'icon-folder-close', 'url': '/admin/app/cabinetsliders/'},
        {'label': u'Услуги', 'icon': 'icon-folder-close', 'url': '/admin/app/services/'},
        {'label': u'Специалисты', 'icon': 'icon-folder-close', 'url': '/admin/app/specialists/'},
        {'label': u'Пациентам', 'icon': 'icon-folder-close', 'url': '/admin/app/pages/'},

    ),
}
EMAIL_HOST = custom_config.get('email', 'smtp')
EMAIL_HOST_PASSWORD = custom_config.get('email', 'password')
EMAIL_HOST_USER = custom_config.get('email', 'user')
EMAIL_PORT = 465
EMAIL_USE_TLS = False
EMAIL_USE_SSL = True

ERROR_LOG_FILE = BASE_DIR + '/logs/' + 'error.log'
INFO_LOG_FILE = BASE_DIR + '/logs/' + 'info.log'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
        'production_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': INFO_LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 7,
            'formatter': 'main_formatter',
            'filters': ['require_debug_false'],
        },
        'debug_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': ERROR_LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 7,
            'formatter': 'main_formatter',
            'filters': ['require_debug_true'],
        },
        'null': {
            "class": 'django.utils.log.NullHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['null', ],
        },
        'py.warnings': {
            'handlers': ['null', ],
        },
        '': {
            'handlers': ['console', 'production_file', 'debug_file'],
            'level': "DEBUG",
        },
    }
}

THUMBNAIL_PROCESSORS = (
                           'image_cropping.thumbnail_processors.crop_corners',
                       ) + thumbnail_settings.THUMBNAIL_PROCESSORS
IMAGE_CROPPING_SIZE_WARNING = True
CKEDITOR_UPLOAD_PATH = "uploads/"