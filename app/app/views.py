# -*- coding: utf-8 -*-
import json
from django.template.response import TemplateResponse, HttpResponse
from django.http import HttpResponsePermanentRedirect
from models import Specialists
from models import Pages
from models import Sliders
from models import ClinicImages
from models import Cabinets
from forms import MessageForm
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from models import Clinics
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import render_to_string


def json_response(x):
    return HttpResponse(json.dumps(x, sort_keys=True, indent=2), content_type='application/json; charset=UTF-8')


def index(request):
    return HttpResponsePermanentRedirect('/clinics/1/')


def clinics(request, id=None):
    if not id:
        id = 1
    clinics = Clinics.objects.all
    clinic = Clinics.objects.get(pk=id)
    images = ClinicImages.objects.all().filter(clinic_id=id).order_by('order')
    specialists = Specialists.objects.all().filter(clinic_id=id).order_by('order')
    cabinets = Cabinets.objects.all().filter(clinic_id=id).order_by('order')
    data = {'clinic': clinic, 'specialists': specialists, 'clinics': clinics, 'images': images, 'cabinets': cabinets}
    pages = Pages.objects.all().filter(clinic_id=id).order_by('order')
    data['pages'] = pages
    slider1 = Sliders.objects.all().filter(status=True, clinic_id=id).order_by('order')[0:3]
    if slider1:
        data['slider1'] = slider1
    slider2 = Sliders.objects.all().filter(status=True, clinic_id=id).order_by('order')[3:6]
    if slider2:
        data['slider2'] = slider2
    slider3 = Sliders.objects.all().filter(status=True, clinic_id=id).order_by('order')[6:9]
    if slider3:
        data['slider3'] = slider3
    return TemplateResponse(request, 'public/index.html', data)


@csrf_exempt
def contact(request):
    if request.method == 'POST':
        form = MessageForm(data=request.POST)
        if form.is_valid():
            model = form
            try:
                clinic = Clinics.objects.get(pk=model.cleaned_data['id'])
                c = Context({'model': model})
                text_content = render_to_string('mail/email.txt', c)
                html_content = render_to_string('mail/email.html', c)
                email = EmailMultiAlternatives(u'Запись на прием', text_content, 'support@voronkovd.ru')
                email.attach_alternative(html_content, "text/html")
                email.to = [clinic.email]
                email.send()
                data = {'status': True, 'message': u'Ваша заявка принята. Мы свяжемся с вами в ближайшее время.'}
            except Exception as e:
                data = {'status': False, 'message': '', 'errors': e.message}
        else:
            data = {'status': False, 'message': '', 'errors': form.errors}
        return json_response(data)
    else:
        raise Http404