# -*- coding: utf-8 -*-
from models import Clinics
from models import ClinicImages
from models import Cabinets
from models import CabinetSliders
from models import Services
from models import Specialists
from models import Pages
from models import Sliders
from django.contrib import admin
from django.forms import ModelForm
from django.forms.widgets import Textarea
from django.contrib.admin import ModelAdmin
from image_cropping import ImageCroppingMixin
from suit.admin import SortableModelAdmin
from ckeditor.widgets import CKEditorWidget
from suit.widgets import LinkedSelect


class ClinicsForm(ModelForm):
    class Meta:
        widgets = {'map': Textarea(attrs={'rows': 3, 'class': 'input-xxlarge'}), 'description': CKEditorWidget()}


class ClinicsAdmin(ImageCroppingMixin, ModelAdmin):
    form = ClinicsForm
    list_display = ('name', 'phone', 'address', 'timetable', 'email', 'image_tag')
    list_filter = ('name', 'phone', 'address', 'timetable', 'email')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False
    show_save_as_new = False
    show_delete_link = False
    show_save_and_add_another = False
    show_save_and_continue = False


class ClinicImagesForm(ModelForm):
    class Meta:
        widgets = {'clinic': LinkedSelect()}


class ClinicImagesAdmin(ImageCroppingMixin, SortableModelAdmin, ModelAdmin):
    form = ClinicImagesForm
    list_display = ('clinic', 'image_tag')
    list_filter = ('clinic',)
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False
    show_save_as_new = False
    show_delete_link = False
    show_save_and_add_another = False
    show_save_and_continue = False
    sortable = 'order'


class CabinetsForm(ModelForm):
    class Meta:
        widgets = {'clinic': LinkedSelect()}


class CabinetsAdmin(ImageCroppingMixin, SortableModelAdmin, ModelAdmin):
    form = CabinetsForm
    list_display = ('clinic', 'name', 'image_tag')
    list_filter = ('clinic', 'name')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False
    show_save_as_new = False
    show_delete_link = False
    show_save_and_add_another = False
    show_save_and_continue = False
    sortable = 'order'


class CabinetSlidersForm(ModelForm):
    class Meta:
        widgets = {'cabinet': LinkedSelect()}


class CabinetSlidersAdmin(ImageCroppingMixin, SortableModelAdmin, ModelAdmin):
    form = CabinetSlidersForm
    list_display = ('cabinet', 'image_tag', 'status')
    list_filter = ('cabinet', 'status')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False
    show_save_as_new = False
    show_delete_link = False
    show_save_and_add_another = False
    show_save_and_continue = False
    sortable = 'order'


class ServicesForm(ModelForm):
    class Meta:
        widgets = {'description': CKEditorWidget(), 'clinic': LinkedSelect()}


class ServicesAdmin(SortableModelAdmin, ModelAdmin):
    form = ServicesForm
    list_display = ('cabinet', 'name')
    list_filter = ('cabinet', 'name')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False
    show_save_as_new = False
    show_delete_link = False
    show_save_and_add_another = False
    show_save_and_continue = False
    sortable = 'order'


class SpecialistsForm(ModelForm):
    class Meta:
        widgets = {'description': CKEditorWidget(), 'clinic': LinkedSelect()}


class SpecialistsAdmin(ImageCroppingMixin, SortableModelAdmin, ModelAdmin):
    form = SpecialistsForm
    list_display = ('clinic', 'name', 'role', 'image_tag')
    list_filter = ('clinic', 'name', 'role')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False
    show_save_as_new = False
    show_delete_link = False
    show_save_and_add_another = False
    show_save_and_continue = False
    sortable = 'order'


class PagesForm(ModelForm):
    class Meta:
        widgets = {'description': CKEditorWidget(), 'clinic': LinkedSelect()}


class PagesAdmin(ImageCroppingMixin, SortableModelAdmin, ModelAdmin):
    form = PagesForm
    list_display = ('clinic', 'title', 'image_tag', 'status')
    list_filter = ('clinic', 'title', 'status')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False
    show_save_as_new = False
    show_delete_link = False
    show_save_and_add_another = False
    show_save_and_continue = False
    sortable = 'order'


class SlidersForm(ModelForm):
    class Meta:
        widgets = {'description': CKEditorWidget(), 'clinic': LinkedSelect()}


class SlidersAdmin(ImageCroppingMixin, SortableModelAdmin, ModelAdmin):
    form = SlidersForm
    list_display = ('clinic', 'title', 'anons', 'image_tag', 'status')
    list_filter = ('clinic', 'title', 'status')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = False
    show_save_as_new = False
    show_delete_link = False
    show_save_and_add_another = False
    show_save_and_continue = False
    sortable = 'order'


admin.site.register(Clinics, ClinicsAdmin)
admin.site.register(ClinicImages, ClinicImagesAdmin)
admin.site.register(Cabinets, CabinetsAdmin)
admin.site.register(CabinetSliders, CabinetSlidersAdmin)
admin.site.register(Services, ServicesAdmin)
admin.site.register(Specialists, SpecialistsAdmin)
admin.site.register(Pages, PagesAdmin)
admin.site.register(Sliders, SlidersAdmin)