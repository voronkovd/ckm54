# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20150410_2250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clinics',
            name=b'cropping',
            field=image_cropping.fields.ImageRatioField(b'image_about', '293x350', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u0424\u043e\u0442\u043e \u0432 \u043f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u044f\u0445'),
            preserve_default=True,
        ),
    ]
