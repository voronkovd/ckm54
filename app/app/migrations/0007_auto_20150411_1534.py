# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_specialists_role'),
    ]

    operations = [
        migrations.AddField(
            model_name='pages',
            name=b'cropping_slide',
            field=image_cropping.fields.ImageRatioField(b'image', '290x120', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='clinics',
            name='price',
            field=models.FileField(help_text='*.xls', upload_to=None, verbose_name='\u041f\u0440\u0430\u0439\u0441'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pages',
            name=b'cropping',
            field=image_cropping.fields.ImageRatioField(b'image', '500x300', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f'),
            preserve_default=True,
        ),
    ]
