# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20150411_1534'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='clinics',
            name='image_about',
        ),
    ]
