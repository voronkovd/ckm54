# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_auto_20150419_2029'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='clinics',
            name=b'cropping',
        ),
        migrations.AddField(
            model_name='clinicimages',
            name='order',
            field=models.PositiveIntegerField(null=True, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430'),
            preserve_default=True,
        ),
    ]
