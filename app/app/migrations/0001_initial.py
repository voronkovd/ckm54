# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cabinets',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435 \u043a\u0430\u0431\u0438\u043d\u0435\u0442\u0430')),
                ('image', models.ImageField(help_text='\u0424\u043e\u0442\u043e \u043a\u0430\u0431\u0438\u043d\u0435\u0442\u0430', upload_to=None, verbose_name='\u0424\u043e\u0442\u043e')),
                (b'cropping', image_cropping.fields.ImageRatioField(b'image', '451x346', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
            ],
            options={
                'db_table': 'cabinets',
                'verbose_name': '\u041a\u0430\u0431\u0438\u043d\u0435\u0442',
                'verbose_name_plural': '\u041a\u0430\u0431\u0438\u043d\u0435\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ClinicImages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(help_text='\u0412 "\u041e \u043d\u0430\u0441"', upload_to=None, verbose_name='\u0424\u043e\u0442\u043e')),
                (b'cropping', image_cropping.fields.ImageRatioField(b'image', '293x214', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                (b'cropping_full', image_cropping.fields.ImageRatioField(b'image', '800x600', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u043f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0443\u0432\u0435\u043b\u0438\u0447\u0435\u043d\u043d\u043e\u0433\u043e \u0444\u043e\u0442\u043e')),
            ],
            options={
                'db_table': 'clinic_galleries',
                'verbose_name': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044e',
                'verbose_name_plural': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Clinics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='\u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: \u041a\u043b\u0438\u043d\u0438\u043a\u0430 \u041c\u0435\u0448\u0430\u043b\u043a\u0438\u043d\u0430', max_length=128, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('phone', models.CharField(help_text='\u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: +7 (383) 100 10 10', max_length=128, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('address', models.CharField(help_text='\u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: \u0443\u043b. \u041a\u0440\u044b\u043b\u043e\u0432\u0430, 1\u0430', max_length=128, verbose_name='\u0410\u0434\u0440\u0435\u0441')),
                ('timetable', models.CharField(help_text='\u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: \u0441 08:00 \u0434\u043e 20:00', max_length=128, verbose_name='\u0413\u0440\u0430\u0444\u0438\u043a')),
                ('email', models.EmailField(help_text='\u0410\u0434\u0440\u0435\u0441 \u0434\u043b\u044f \u0437\u0430\u043f\u0438\u0441\u0438 \u043d\u0430 \u043f\u0440\u0438\u0435\u043c', max_length=75, verbose_name='Email')),
                ('description', models.TextField(help_text='\u0412 \u0440\u0430\u0437\u0434\u0435\u043b\u0435 "\u041e \u043d\u0430\u0441"', verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('map', models.TextField(help_text='https://tech.yandex.ru/maps/tools/constructor/', null=True, verbose_name='\u041a\u043e\u0434')),
                ('price', models.FileField(help_text='*.xls', upload_to=None)),
                ('logo', models.ImageField(help_text='230x60', upload_to=None, verbose_name='\u041b\u043e\u0433\u043e')),
                ('logo_s', models.ImageField(help_text='230x60', upload_to=None, verbose_name='\u041b\u043e\u0433\u043e \u043c\u0435\u043d\u044c\u0448\u0435')),
                ('image_about', models.ImageField(upload_to=None, verbose_name='\u0424\u043e\u0442\u043e \u0432 "\u041e \u043d\u0430\u0441"')),
                (b'cropping', image_cropping.fields.ImageRatioField(b'image_about', '293x350', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u0424\u043e\u0442\u043e \u0432 \u043f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u044f\u0445')),
            ],
            options={
                'db_table': 'clinics',
                'verbose_name': '\u041a\u043b\u0438\u043d\u0438\u043a\u0443',
                'verbose_name_plural': '\u041a\u043b\u0438\u043d\u0438\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u0442\u044c')),
                ('title', models.CharField(help_text='\u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: \u0414\u0438\u0441\u043a\u043e\u043d\u0442\u043d\u0430\u044f \u0441\u0438\u0441\u0442\u0435\u043c\u0430', max_length=128, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0424\u043e\u0442\u043e')),
                ('description', models.TextField(verbose_name='\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430')),
                (b'cropping', image_cropping.fields.ImageRatioField(b'image', '500x300', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('slider', models.BooleanField(default=False, verbose_name='\u0412 \u0441\u043b\u0430\u0439\u0434\u0435\u0440')),
                ('clinic', models.ForeignKey(verbose_name='\u041a\u043b\u0438\u043d\u0438\u043a\u0430', to='app.Clinics')),
            ],
            options={
                'db_table': 'pages',
                'verbose_name': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u0432 \u0440\u0430\u0437\u0434\u0435\u043b \u043f\u0430\u0446\u0438\u0435\u043d\u0442\u0430\u043c',
                'verbose_name_plural': '\u041f\u0430\u0446\u0438\u0435\u043d\u0442\u0430\u043c',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Services',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435 \u0443\u0441\u043b\u0443\u0433\u0443')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('cabinet', models.ForeignKey(related_name='services', verbose_name='\u041a\u0430\u0431\u0438\u043d\u0435\u0442', to='app.Cabinets')),
            ],
            options={
                'db_table': 'services',
                'verbose_name': '\u0423\u0441\u043b\u0443\u0433\u0443',
                'verbose_name_plural': '\u0423\u0441\u043b\u0443\u0433\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Specialists',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='\u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: \u0418\u0432\u0430\u043d\u043e\u0432 \u0418\u0432\u0430\u043d \u0418\u0432\u0430\u043d\u043e\u0432\u0438\u0447', max_length=128, verbose_name='\u0424\u0418\u041e')),
                ('description', models.TextField(help_text='\u041e \u0441\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0441\u0442\u0435', verbose_name='\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f')),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0424\u043e\u0442\u043e')),
                (b'cropping', image_cropping.fields.ImageRatioField(b'image', '250x380', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('clinic', models.ForeignKey(verbose_name='\u041a\u043b\u0438\u043d\u0438\u043a\u0430', to='app.Clinics')),
            ],
            options={
                'db_table': 'specialists',
                'verbose_name': '\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0441\u0442\u0430',
                'verbose_name_plural': '\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0441\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='clinicimages',
            name='clinic',
            field=models.ForeignKey(verbose_name='\u041a\u043b\u0438\u043d\u0438\u043a\u0430', to='app.Clinics'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cabinets',
            name='clinic',
            field=models.ForeignKey(verbose_name='\u041a\u043b\u0438\u043d\u0438\u043a\u0430', to='app.Clinics'),
            preserve_default=True,
        ),
    ]
