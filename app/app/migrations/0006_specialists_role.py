# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20150411_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='specialists',
            name='role',
            field=models.CharField(help_text='\u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: \u0425\u0438\u0440\u0443\u0440\u0433', max_length=128, null=True, verbose_name='\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c'),
            preserve_default=True,
        ),
    ]
