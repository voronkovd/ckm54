# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_remove_clinics_image_about'),
    ]

    operations = [
        migrations.AddField(
            model_name='cabinets',
            name='order',
            field=models.PositiveIntegerField(null=True),
            preserve_default=True,
        ),
    ]
