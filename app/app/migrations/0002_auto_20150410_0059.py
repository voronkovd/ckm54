# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='clinicimages',
            name=b'cropping_full',
            field=image_cropping.fields.ImageRatioField(b'image', '800x600', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u043f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0443\u0432\u0435\u043b\u0438\u0447\u0435\u043d\u043d\u043e\u0433\u043e \u0444\u043e\u0442\u043e'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='clinicimages',
            name=b'cropping',
            field=image_cropping.fields.ImageRatioField(b'image', '293x214', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='clinics',
            name=b'cropping',
            field=image_cropping.fields.ImageRatioField(b'image_about', '229x286', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u0424\u043e\u0442\u043e \u0432 \u043f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u044f\u0445'),
            preserve_default=True,
        ),
    ]
