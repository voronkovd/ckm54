# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20150410_0059'),
    ]

    operations = [
        migrations.AddField(
            model_name='clinics',
            name='map',
            field=models.TextField(help_text='https://tech.yandex.ru/maps/tools/constructor/', null=True, verbose_name='\u041a\u043e\u0434'),
            preserve_default=True,
        ),
    ]
