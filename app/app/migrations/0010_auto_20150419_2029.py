# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0009_cabinets_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='pages',
            name='order',
            field=models.PositiveIntegerField(null=True, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='services',
            name='order',
            field=models.PositiveIntegerField(null=True, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='specialists',
            name='order',
            field=models.PositiveIntegerField(null=True, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cabinets',
            name='order',
            field=models.PositiveIntegerField(null=True, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430'),
            preserve_default=True,
        ),
    ]
