# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20150419_2214'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sliders',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='\u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: \u041f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u044b \u0438 \u0430\u043a\u0446\u0438\u0438', max_length=24, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('anons', models.TextField(max_length=50, verbose_name='\u0410\u043d\u043e\u043d\u0441')),
                ('description', models.TextField(verbose_name='\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f')),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                (b'cropping', image_cropping.fields.ImageRatioField(b'image', '290x120', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f')),
                ('order', models.PositiveIntegerField(null=True, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430')),
                ('status', models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u0442\u044c')),
                ('clinic', models.ForeignKey(verbose_name='\u041a\u043b\u0438\u043d\u0438\u043a\u0430', to='app.Clinics')),
            ],
            options={
                'db_table': 'sliders',
                'verbose_name': '\u0421\u043b\u0430\u0439\u0434',
                'verbose_name_plural': '\u0421\u043b\u0430\u0434\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='pages',
            name='slider',
        ),
        migrations.AlterField(
            model_name='clinics',
            name='description',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='specialists',
            name='description',
            field=models.TextField(verbose_name='\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f'),
            preserve_default=True,
        ),
    ]
