# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0012_auto_20150419_2346'),
    ]

    operations = [
        migrations.CreateModel(
            name='CabinetSliders',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                (b'cropping', image_cropping.fields.ImageRatioField(b'image', '451x346', hide_image_field=False, size_warning=True, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='\u041f\u0440\u043e\u043f\u043e\u0440\u0446\u0438\u0438 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('order', models.PositiveIntegerField(null=True, verbose_name='\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430')),
                ('status', models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u0442\u044c')),
                ('cabinet', models.ForeignKey(verbose_name='\u041a\u0430\u0431\u0438\u043d\u0435\u0442', to='app.Cabinets')),
            ],
            options={
                'db_table': 'cabinet_sliders',
                'verbose_name': '\u0421\u043b\u0430\u0439\u0434 \u043a\u0430\u0431\u0438\u043d\u0435\u0442\u0430',
                'verbose_name_plural': '\u0421\u043b\u0430\u0434\u044b \u043a\u0430\u0431\u0438\u043d\u0435\u0442\u0430',
            },
            bases=(models.Model,),
        ),
    ]
