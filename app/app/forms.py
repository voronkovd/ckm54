# -*- coding: utf-8 -*-
from django import forms


class MessageForm(forms.Form):
    name = forms.CharField(max_length=36)
    phone = forms.CharField(max_length=16)
    content = forms.Textarea()
    id = forms.IntegerField()